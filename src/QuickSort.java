import java.util.Arrays;

public class QuickSort {
    public static void main(String[] args) {
        int[] array = {5, 4, 7, 1, 3, 9, 8, 2, 6, 0};
        System.out.println( Arrays.toString( array ) );

        int low = 0;
        int high = array.length - 1;

        quickSort( array, low, high );
        System.out.println( Arrays.toString( array ) );
    }

    /**
     * Сортирует масиив{@code array}
     *
     * @param array массив целых чисел
     * @param low   начало массива
     * @param high  конец массива
     */
    private static void quickSort(int[] array, int low, int high) {
        if (array.length == 0) {
            System.out.println( "массив пуст" );
            System.exit( 0 );
        }

        if (low >= high) {
            System.exit( 0 );
        }

        //опорный элемент
        int middle = (high + low) / 2;
        int support = array[middle];

        int i = low;
        int j = high;
        while (i <= j) {
            while (array[i] < support) {
                i++;
            }

            while (array[j] > support) {
                j--;
            }

            if (i <= j) {
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
                i++;
                j--;
            }
        }

        if (low < j) {
            quickSort( array, low, j );
        }

        if (high > i) {
            quickSort( array, i, high );
        }
    }
}


